package com.rikkei.tranning.project;

import android.app.ActivityGroup;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;

public class MainActivity extends ActivityGroup {
    TabHost mTabhost;
//    DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTabhost = findViewById(android.R.id.tabhost);
        mTabhost.setup( this.getLocalActivityManager() );
        createTab();
        // create data test
//        dbManager = new DBManager(this);
//        for ( int  i = 160; i < 170; i++ ){
//            User user = new User(i, "nguyenvan" + i + "@gmail.com", "12345678", "Nguyen Van " + i);
//            dbManager.addUser( user );
//        }
//
//        Log.d("Testing123", dbManager.getRecordCount("users") + "");


    }

    public void createTab(){
        // home
        TabHost.TabSpec home = mTabhost.newTabSpec("Home");
        home.setIndicator("Home");
        home.setContent( new Intent(MainActivity.this, Home.class));

        // near
        TabHost.TabSpec near = mTabhost.newTabSpec("Near");
        near.setIndicator("Near");
        near.setContent( new Intent(MainActivity.this, Near.class));

        // browser
        TabHost.TabSpec browser = mTabhost.newTabSpec("Browse");
        browser.setIndicator("Browse");
        browser.setContent( new Intent(MainActivity.this, Browser.class));

        // me
        TabHost.TabSpec me = mTabhost.newTabSpec("Me");
        me.setIndicator("Me");
        me.setContent( new Intent(MainActivity.this, doRegisterAPI.class));

        //
        mTabhost.addTab( home );
        mTabhost.addTab( near );
        mTabhost.addTab( browser );
        mTabhost.addTab( me );
        mTabhost.setCurrentTab(0);
    }
}
