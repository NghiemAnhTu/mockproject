package com.rikkei.tranning.project;

import java.sql.Date;

public class New {
    int news_id;
    String feed;
    String title;
    String detail_url;
    String thumb_img;
    String description;
    String author;
    int publist_date;


    public New(){}


    public New(int news_id, String feed, String title, String detail_url, String thumb_img, String description, String author, int publist_date) {
        this.news_id = news_id;
        this.feed = feed;
        this.title = title;
        this.detail_url = detail_url;
        this.thumb_img = thumb_img;
        this.description = description;
        this.author = author;
        this.publist_date = publist_date;
    }


    public int getNews_id() {
        return news_id;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail_url() {
        return detail_url;
    }

    public void setDetail_url(String detail_url) {
        this.detail_url = detail_url;
    }

    public String getThumb_img() {
        return thumb_img;
    }

    public void setThumb_img(String thumb_img) {
        this.thumb_img = thumb_img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPublist_date() {
        return publist_date;
    }

    public void setPublist_date(int publist_date) {
        this.publist_date = publist_date;
    }
}
