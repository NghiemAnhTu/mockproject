package com.rikkei.tranning.project;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.text.format.DateFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DBManager1 extends SQLiteOpenHelper {
    private final Context mContext;
    private static  String DATABASE_NAME = "tokyo_art_beat.db";
    private static String DATABASE_PATH = "/data/data/com.rikkei.tranning.project/";
    private SQLiteDatabase mMyDatabase;


    public DBManager1(Context context ) {
        super(context, DATABASE_NAME, null, 1);

        if (Build.VERSION.SDK_INT >= 15 ){
            DATABASE_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }else {
            DATABASE_PATH = Environment.getDataDirectory() + "/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if ( newVersion > oldVersion ){
            try {
                copyDatabase();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //
    public void checkAndCopyData(){
        boolean dbexist = checkDatabase();
        SQLiteDatabase db_read = null;
        if ( !dbexist ){

                db_read = this.getReadableDatabase();

                try {
                    copyDatabase();
                } catch (IOException e) {
                    e.printStackTrace();
                }

        }

    }

    // lay csdl
    public SQLiteDatabase getDatabase(){
        return mMyDatabase;
    }

    // kiem tra csdl
    private boolean checkDatabase(){
        boolean checkdb = false;

        try {
            String myPath = DATABASE_PATH + DATABASE_NAME;
            File dbfile = new File(myPath);
            checkdb = dbfile.exists();
        } catch (SQLiteException e)
        {
            System.out.println("Databse doesn't exist!");
        }

        return checkdb;
    }

    // copy csdl
    private  void copyDatabase() throws IOException{
        AssetManager dirPath = mContext.getAssets();
        InputStream myInput = mContext.getAssets().open(DATABASE_NAME);
        String outFileName = DATABASE_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream("data/data/com.rikkei.tranning.project/tokyo_art_beat.db");
        byte[] buffer = new byte[1024];
        int length = 0;
        while ( (length = myInput.read(buffer)) > 0 ){
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

//    @Override
//    public void onOpen(SQLiteDatabase db) {
//        super.onOpen(db);
//        db.disableWriteAheadLogging();
//    }

        // mơ csdl
    public void openDB() throws SQLException{
        String dbPath = mContext.getDatabasePath(DATABASE_NAME).getPath();
        if (mMyDatabase != null && mMyDatabase.isOpen()) {
            return;
        }
        mMyDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);

//        String myPath = DATABASE_PATH + DATABASE_NAME;
//        mMyDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
//        mMyDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    //
    public synchronized void close(){
        mMyDatabase.close();
        super.close();
    }

    // cap nhat data
    public void queryData( String sql ){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(sql);
    }

    // doc data
    public Cursor getData( String sql){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery(sql, null);
    }


    public ArrayList<New> getAllNews(){
        ArrayList<New> list = new ArrayList<>();

        String query = "SELECT * FROM news";
        Cursor cursor = getData( query );
        if ( cursor.moveToFirst() ){
            do {
                New news = new New();
                news.setNews_id( cursor.getInt(0) );
                news.setFeed( cursor.getString(1));
                news.setTitle( cursor.getString(2));
                news.setDetail_url( cursor.getString(3));
                news.setThumb_img( cursor.getString(4));
                news.setDescription(cursor.getString(5));
                news.setAuthor(cursor.getString(6));
                news.setPublist_date(  cursor.getInt(7));
            }while ( cursor.moveToNext());
        }
        cursor.close();
//        db.close();
        return  list;

    }
}
