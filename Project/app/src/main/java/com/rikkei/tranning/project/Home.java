package com.rikkei.tranning.project;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class Home extends ActivityGroup {
    TabHost mTabHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mTabHome = findViewById(android.R.id.tabhost);
        mTabHome.setup( this.getLocalActivityManager());
        createTabHome();



    }

    public void createTabHome(){
        // tab ListNew
        TabHost.TabSpec news = mTabHome.newTabSpec("News");
        news.setContent( new Intent(Home.this, ListNew.class));
        news.setIndicator("News");

        // tab ListPopular
        TabHost.TabSpec popular = mTabHome.newTabSpec("Popular");
        popular.setContent( new Intent(Home.this, ListPopular.class));
        popular.setIndicator("Popular");

        mTabHome.addTab(news);
        mTabHome.addTab(popular);
        mTabHome.setCurrentTab(0);

    }
}
