//package com.rikkei.tranning.project;
//
//import android.arch.paging.LivePagedListProvider;
//import android.arch.persistence.room.ColumnInfo;
//import android.arch.persistence.room.Dao;
//import android.arch.persistence.room.Entity;
//import android.arch.persistence.room.Insert;
//import android.arch.persistence.room.OnConflictStrategy;
//import android.arch.persistence.room.PrimaryKey;
//import android.arch.persistence.room.Query;
//
//import java.util.List;
//
//@Entity
//public class News {
//    @PrimaryKey(autoGenerate = true)
//    @ColumnInfo(name = "News_id")
//    public long news_id;
//    @ColumnInfo(name = "Feed")
//    public String feed;
//    @ColumnInfo(name = "Title")
//    public String title;
//    @ColumnInfo(name = "Thumb_img")
//    public String thumb_img;
//    @ColumnInfo(name = "Description")
//    public String description;
//    @ColumnInfo(name = "Author")
//    public String author;
//    @ColumnInfo(name = "Publish_date")
//    public long publish_date;
//}
//
//@Dao
//interface NewDao{
//    @Insert( onConflict = OnConflictStrategy.REPLACE)
//    public void insertAll(List<News> newsList );
//
//    @Query("SELECT * FROM News")
//    public abstract LivePagedListProvider<Long, News> newsByFirstName();
//
//}
