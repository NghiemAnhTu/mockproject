package com.rikkei.tranning.project;

interface OnItemClickListener {
    public void onItemClick();
}
