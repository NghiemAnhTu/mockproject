package com.rikkei.tranning.project;

public class Categories {
    int id;
    String name;
    String slug;
    int parent_id;

    //
    public Categories(){}

    public Categories(int id, String name, String slug, int parent_id) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.parent_id = parent_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }
}
