package com.rikkei.tranning.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class doLoginAPI extends AppCompatActivity {
    EditText mEmail, mPassword;
    Button mbtnLogin;
    TextView txtForgotPassword;
    ImageView mBackLogin;
    DBManager dbManager;
    ArrayList<User> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        AnhXa();
        Intent login = getIntent();
        //
        mEmail.addTextChangedListener( LoginTextWatcher );
        mPassword.addTextChangedListener( LoginTextWatcher );
        // lay data
        dbManager = new DBManager(this);
        list = dbManager.getAllUsers();

    }

    //
    private TextWatcher LoginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String email = mEmail.getText().toString().trim();
            String pass = mPassword.getText().toString().trim();

            mbtnLogin.setEnabled( !email.isEmpty() && !pass.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void AnhXa(){
        mEmail = findViewById(R.id.emailLogin);
        mPassword = findViewById(R.id.passwordLogin);
        mbtnLogin = findViewById(R.id.btnLogin);
        txtForgotPassword = findViewById(R.id.textForgotPassword);
        mBackLogin = findViewById(R.id.image_Login);

    }

    // forgot password
    public void ForgotPassword(View v){
        Intent mIForgotPassword = new Intent(doLoginAPI.this, resetPasswordAPI.class);
        startActivity(mIForgotPassword);

    }

    // back sign up
    public void BackSignUp(View v){
        Intent mISignUp = new Intent(doLoginAPI.this, Me.class);
        startActivity(mISignUp);
    }

    // Login
    public void Login(View v){
        String email = mEmail.getText().toString();
        String pass = mPassword.getText().toString();
        //
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        //
        if ( email.matches(emailPattern) && email.length() > 0 ){
            int flag = 0;
            for ( int  i = 0; i < list.size(); i++ ){
                if ( list.get(i).getEmail().equals(email) ){
                    if ( list.get(i).getPassword().equals( pass ) ){
                        flag = 1;
                        break;
                    }else {
                        Toast.makeText(doLoginAPI.this, "Mật khẩu không đung! Vui lòng nhập lại.", Toast.LENGTH_LONG).show();
                        mPassword.setText("");
                    }
                }
            }
            if ( flag == 1 ){
                Intent IHome = new Intent(doLoginAPI.this, Home.class);
                IHome.putExtra("email", email);
                startActivity(IHome);
            }else {
                mEmail.setText("");
                mPassword.setText("");
                Toast.makeText(doLoginAPI.this, "Email không đúng! Vui lòng nhập lại.", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(doLoginAPI.this, "Định dạng email không đúng! Vui lòng nhập lại.", Toast.LENGTH_LONG).show();
        }

    }

}
