package com.rikkei.tranning.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class doRegisterAPI extends AppCompatActivity {
    EditText mName, mEmail, mPassword;
    TextView txtAMember;
    Button mbtnSignup;
    int flag = 0;
    int REQUEST_CODE = 100;
    ArrayList<User> list = new ArrayList<>();
    DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);
        AnhXa();
        mName.addTextChangedListener(SignUpTextWatcher);
        mEmail.addTextChangedListener(SignUpTextWatcher);
        mPassword.addTextChangedListener(SignUpTextWatcher);
        dbManager = new DBManager(this);
        list = dbManager.getAllUsers();
//        mbtnSignup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int id = -1;
//                int flag = 1;
//                String name, email, password;
//                name = mName.getText().toString();
//                email = mEmail.getText().toString();
//                password = mPassword.getText().toString();
//                for ( int i = 0; i < list.size(); i++ ){
//                    if ( list.get(i).getEmail().equals(mEmail) ){
//                        flag = 0;
//                        break;
//                    }
//                }
//                if ( flag == 1 ){
//                    dbManager.addUser( new User(list.size() + 160, email, name, password) );
//                    Intent register = new Intent(doRegisterAPI.this, doLoginAPI.class);
//                    startActivity( register );
//                }else {
//                    mName.setText("");
//                    mEmail.setText("");
//                    mPassword.setText("");
//                    Toast.makeText(doRegisterAPI.this, "Đã tồn tại email. Vui lòng chọn email khác!", Toast.LENGTH_LONG).show();
//                }
//
//
//            }
//        });
//
    }

    private TextWatcher SignUpTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String name = mName.getText().toString().trim();
            String email = mEmail.getText().toString().trim();
            String password = mPassword.getText().toString().trim();

            mbtnSignup.setEnabled( !name.isEmpty() && !email.isEmpty() && !password.isEmpty());

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void AnhXa(){
        txtAMember = findViewById(R.id.textAMember);
        mName = findViewById(R.id.fullname);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mbtnSignup = findViewById(R.id.btnSignUp);

    }

    // have a acount
    public void AMember(View v){
        Intent login = new Intent(doRegisterAPI.this, doLoginAPI.class);
        startActivity(login);
    }

    // sign up
    public void SignUp( View v ){
        int flag = 1;
        String name = mName.getText().toString();
        String password = mPassword.getText().toString();
        String email = mEmail.getText().toString();
        // dinh dang email
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        // dang ki
        if ( email.matches(emailPattern) && email.length() > 0 ){
            if ( name.length() > 0 && password.length() > 6 && password.length() < 14 ){
                for ( int i = 0; i < list.size(); i++ ){
                    if ( list.get(i).getEmail().equals(email) ){
                        flag = 0;
                        break;
                    }
                }
                if ( flag == 1 ){
                    dbManager.addUser( new User(list.size() + 160, email, name, password) );
                    Intent register = new Intent(doRegisterAPI.this, doLoginAPI.class);
                    startActivity( register );
                }else {
                    mName.setText("");
                    mEmail.setText("");
                    mPassword.setText("");
                    Toast.makeText(doRegisterAPI.this, "Đã tồn tại email. Vui lòng chọn email khác!", Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(doRegisterAPI.this, "Vui lòng nhập đầy đủ thông tin, password có dộ dài từ 6-14 kí tự.", Toast.LENGTH_LONG).show();
            }

        }else {
            Toast.makeText(doRegisterAPI.this, "Email không đúng.", Toast.LENGTH_LONG).show();

        }
    }

}