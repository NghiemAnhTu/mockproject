package com.rikkei.tranning.project;

import android.app.ActivityGroup;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;

public class listMyEvents extends ActivityGroup {

    TabHost mTabMyPage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_my_events);
        mTabMyPage = findViewById(android.R.id.tabhost);
        mTabMyPage.setup( this.getLocalActivityManager());
        createMyPage();
    }

    public void createMyPage(){
        // tab Going
        TabHost.TabSpec tabGoing = mTabMyPage.newTabSpec("Going");
        Intent going = new Intent(listMyEvents.this, GoingEvents.class);
        tabGoing.setContent( going );

        TabHost.TabSpec tabWent = mTabMyPage.newTabSpec("Went");
        Intent went = new Intent(listMyEvents.this, WentEvents.class);
        tabGoing.setContent( went );

        mTabMyPage.addTab( tabGoing );
        mTabMyPage.addTab( tabWent );
        mTabMyPage.setCurrentTab(0);

    }
}
