package com.rikkei.tranning.project;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class NewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    ILoadMore loadMore;
    List<New> list = new ArrayList<New>();
    Context mContext;
    boolean isLoading;
    int lastNews, totalNews;
    int visibleThreshold = 10;
    private OnItemClickListener itemClickListener;


    public NewAdapter(List<New> list, Context mContext/**, RecyclerView recyclerView*/) {
        this.list = list;
        this.mContext = mContext;
        // quan li recyclerView
//        final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                // tong news trong list
//                totalNews = layoutManager.getItemCount();
//                // vi tri news cuoi cung trong recyclerview
//                lastNews = layoutManager.findLastVisibleItemPosition();
//                if ( !isLoading && totalNews <= (lastNews + visibleThreshold) ) { // load
//                    if ( loadMore != null ){
//                        loadMore.onLoadMore();
//                        isLoading = true;
//                    }
//                }
//            }
//        });
    }

    public List<New> getList() {
        return list;
    }

    public void setList(List<New> list) {
        this.list = list;
    }

    public OnItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position)  == null ? 1 : 0;
    }

    public void setLoadMore(ILoadMore loadMore){
        this.loadMore = loadMore;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
//        if ( i == 0 ){
            View view = inflater.inflate(R.layout.customer_new, viewGroup, false );
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
//        }else {
//            View view = inflater.inflate(R.layout.item_loading, viewGroup, false );
//            LoadingViewHolder loadingViewHolder = new LoadingViewHolder(view);
//            return loadingViewHolder ;
//        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if ( viewHolder instanceof ViewHolder ){
            final New news = list.get(i);
            ViewHolder holder = (ViewHolder) viewHolder;
            //./..er.imgThumbImg.setImageResource( );
            holder.txtPublishDate.setText( news.getPublist_date() + "");
            holder.txtTitle.setText( news.getTitle() );
            holder.txtFeed.setText( news.getFeed() );
            holder.txtAuthor.setText( news.getAuthor() );
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent newsIntent = new Intent(mContext, doLoginAPI.class);
                    newsIntent.putExtra("New_id", news.getNews_id());
                    mContext.startActivity( newsIntent );
                }
            });

        }else if ( viewHolder instanceof LoadingViewHolder ){
            LoadingViewHolder holder = (LoadingViewHolder) viewHolder;
            holder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setLoader(){
        isLoading = false;
    }

}

class ViewHolder extends RecyclerView.ViewHolder /**implements View.OnClickListener, View.OnLongClickListener*/{
    TextView txtTitle, txtAuthor, txtPublishDate, txtFeed;
    ImageView imgThumbImg;
    LinearLayout linearLayout;


    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        this.imgThumbImg = itemView.findViewById(R.id.image_thumb);
        this.txtAuthor = itemView.findViewById(R.id.author_news);
        this.txtFeed = itemView.findViewById(R.id.feed_news);
        this.txtPublishDate = itemView.findViewById(R.id.publish_date_news);
        this.txtTitle = itemView.findViewById(R.id.title_news);
        this.linearLayout = itemView.findViewById(R.id.lineralayout);
    }
}

class LoadingViewHolder extends RecyclerView.ViewHolder{
    ProgressBar progressBar;

    public LoadingViewHolder(@NonNull View itemView) {
        super(itemView);
        this.progressBar = itemView.findViewById(R.id.progressBar);
    }
}
