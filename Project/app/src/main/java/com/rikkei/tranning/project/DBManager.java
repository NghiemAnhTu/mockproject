package com.rikkei.tranning.project;

import android.accounts.NetworkErrorException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Date;
import java.util.ArrayList;

public class DBManager extends SQLiteOpenHelper {
    private Context mContext;
    private static  String DB_NAME = "TokyoArtBeat";

    public DBManager(Context context ) {
        super(context, DB_NAME, null, 1);
        this.mContext = mContext;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        // create table categories
        String categories = "CREATE TABLE categories (" +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "name" + " VARCHAR(128)," +
                "slug" + " VARCHAR(128)," +
                "parent_id" + " INTEGER(11)," +
                "created_at" + " DATE," +
                "updated_at" + " DATE );";
        // create table events
        String events = "CREATE TABLE events (" +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "status" + " INTEGER," +
                "link" + " VARCHAR(255)," +
                "photo" + " VARCHAR(255)," +
                "name" + " VARCHAR(128)," +
                "description_raw" + " VARCHAR(255)," +
                "description_html" + " VARCHAR(255)," +
                "artist" + " BLOB," +
                "artist_etc" + " VARCHAR(255)," +
                "image_copyright" + " VARCHAR(128)," +
                "one_category_link" + " VARCHAR(255) ," +
                "one_category_name" + " VARCHAR(255)," +
                "url" + " VARCHAR(255)," +
                "fee_description" + " VARCHAR(128)," +
                "fee_discount" + " INTEGER(11)," +
                "fee_discount_details" + " VARCHAR(255)," +
                "schedule_permanent" + " VARCHAR(255)," +
                "schedule_date_warning" + " VARCHAR(255)," +
                "schedule_time_alert" + " VARCHAR(128)," +
                "schedule_start_date" + " VARCHAR(255)," +
                "schedule_start_time" + " VARCHAR(255)," +
                "schedule_end_date" + " VARCHAR(255)," +
                "schedule_end_time" + " VARCHAR(255)," +
                "schedule_one_day_event" + " VARCHAR(255)," +
                "schedule_extra" + " VARCHAR(255)," +
                "venue_id" + " VARCHAR(8)," +
                "created_at" + " DATE," +
                "updated_at" + " DATE," +
                "CONSTRAINT fk_events FOREIGN KEY(venue_id) REFERENCES venues (id) ON DELETE CASCADE ON UPDATE CASCADE);";

        // create table events_categories
        String events_categories = "CREATE TABLE events_categories ( " +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "events_id" + " INTEGER(11)," +
                "category_id" + " INTEGER(11)," +
                "created_at" + " DATE," +
                "updated_at" + " DATE," +
                "CONSTRAINT fk_events_categories_1 FOREIGN KEY(events_id) REFERENCES events (id) ON DELETE CASCADE ON UPDATE CASCADE," +
                "CONSTRAINT fk_events_categories_2 FOREIGN KEY(category_id) REFERENCES catogories (id) ON DELETE CASCADE ON UPDATE CASCADE);";
        // create table news
        String news = "CREATE TABLE news (" +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "feed" + " VARCHAR(128)," +
                "title" + " VARCHAR(255)," +
                "details_url" + " VARCHAR(255)," +
                "thumb" + " VARCHAR(128)," +
                "description" + " VARCHAR(255)," +
                "author" + " VARCHAR(128)," +
                "publish_date" + " INTEGER ," +
                "created_at" + " DATE," +
                "updated_at" + " DATE );";
        // create users
        String users = "CREATE TABLE users (" +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "email" + " VARCHAR(128)," +
                "password" + " VARCHAR(255)," +
                "name" + " VARCHAR(255)," +
                "created_at" + " DATE," +
                "updated_at" + " DATE );";
        // create table user_events
        String user_events = "CREATE TABLE user_events (" +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "event_id" + " INTEGER(11)," +
                "user_id" + " INTEGER(11)," +
                "status" + " INTEGER(6)," +
                "created_at" + " DATE," +
                "updated_at" + " DATE," +
                "CONSTRAINT fk_user_events_1 FOREIGN KEY(event_id) REFERENCES events (id) ON DELETE CASCADE ON UPDATE CASCADE," +
                "CONSTRAINT fk_user_events_2 FOREIGN KEY(user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE);";

        // create table venues
        String venues = "CREATE TABLE venues (" +
                "id" + " INTEGER(11) PRIMARY KEY," +
                "name" + " VARCHAR(255)," +
                "type" + " INTEGER(6)," +
                "description" + " TEXT," +
                "permanent" + " INTEGER(11)," +
                "contact_fee" + " VARCHAR(255)," +
                "contact_phone" + " VARCHAR(255)," +
                "contact_fax" + " VARCHAR(20)," +
                "contact_web" + " VARCHAR(255)," +
                "contact_web_lang" + " VARCHAR(255)," +
                "contact_address" + " VARCHAR(255) ," +
                "contact_access" + " VARCHAR(255)," +
                "contact_discount" + " INTEGER(11)," +
                "contact_discount_details" + " VARCHAR(128)," +
                "geo_are" + " VARCHAR(255)," +
                "geo_long" + " DECIMAL(9, 6)," +
                "geo_lat" + " DECIMAL(8, 6)," +
                "schedule_openinghour" + " TIME," +
                "schedule_closinghour" + " TIME," +
                "schedule_breakstart" + " BOOLEAN," +
                "schedule_breakend" + " BOOLEAN," +
                "schedule_openingdetails" + " VARCHAR(255)," +
                "schedule_closed" + " BLOB," +
                "created_at" + " DATE," +
                "updated_at" + " DATE );";
        // execSQL
        sqLiteDatabase.execSQL(categories);
        sqLiteDatabase.execSQL(events);
        sqLiteDatabase.execSQL(events_categories);
        sqLiteDatabase.execSQL(news);
        sqLiteDatabase.execSQL(users);
        sqLiteDatabase.execSQL(user_events);
        sqLiteDatabase.execSQL(venues);
//        Log.d("Testing1", getNewsCount() + "");
//        //createNews();
//        Log.d("Testing2", getNewsCount( ) + "");



        // add foregin key

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql1 = "DROP TABLE IF EXISTS user_events;";
        String sql2 = "DROP TABLE events;";
        String sql3 = "DROP TABLE users;";
        String sql4 = "DROP TABLE news;";
        String sql5 = "DROP TABLE categories;";
        String sql6 = "DROP TABLE venues;";

        sqLiteDatabase.execSQL(sql1);
        sqLiteDatabase.execSQL(sql2);
        sqLiteDatabase.execSQL(sql3);
        sqLiteDatabase.execSQL(sql4);
        sqLiteDatabase.execSQL(sql5);
        sqLiteDatabase.execSQL(sql6);
        onCreate( sqLiteDatabase );

    }

    // create news
    public void createNews(){
        int count = getRecordCount("news");
        if ( count == 0 ){
            for ( int i = 1; i < 15; i++){
                New a = new New(i,"name feed " + i, "name title " + i, "name detail_url " + i, "a", "name description " + i, "name author " + i, i);
                addNews( a );
            }
        }
    }

    // add News
    public void addNews( New news ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", news.getNews_id());
        values.put("feed", news.getFeed());
        values.put("title", news.getTitle());
        values.put("details_url", news.getDetail_url());
        values.put("thumb", news.getThumb_img());
        values.put("description", news.getDescription());
        values.put("author", news.getAuthor());
        values.put("publish_date", news.getPublist_date());
        values.put("created_at", "2019-09-03");
        values.put("updated_at", "2019-09-04");
        // add one news at databases
        db.insert("news", null, values);
        // disconnect
        db.close();
    }

    // get a new
    public New getNew( int news_id ){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("news", new String[]{"id", "feed", "title", "detail_url", "thumb", "description", "author", "publish_date", "created_at", "updated_at"},
                "id=?", new  String[]{String.valueOf(news_id)},null, null, null, null);
        New tmp = new New();
        if ( cursor != null ) {
            cursor.moveToFirst();
            // do data ra
            tmp.setNews_id(Integer.parseInt(cursor.getString(0)));
            tmp.setFeed(cursor.getString(1));
            tmp.setTitle(cursor.getString(2));
            tmp.setDetail_url(cursor.getString(3));
            tmp.setThumb_img(cursor.getString(4));
            tmp.setDescription(cursor.getString(5));
            tmp.setAuthor(cursor.getString(6));
            tmp.setPublist_date(Integer.parseInt(cursor.getString(7)));
        }
        return tmp;
    }

    // get all news
    public ArrayList<New> getAllNews() {
        ArrayList<New> list = new ArrayList<New>();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM news order by created_at DESC;";
        // duyet con tro va add vao Arraylist
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                New news = new New();
                news.setNews_id(Integer.parseInt(cursor.getString(0)));
                news.setFeed(cursor.getString(1));
                news.setTitle(cursor.getString(2));
                news.setDetail_url(cursor.getString(3));
                news.setThumb_img(cursor.getString(4));
                news.setDescription(cursor.getString(5));
                news.setAuthor(cursor.getString(6));
                news.setPublist_date(Integer.parseInt(cursor.getString(7)));
                list.add( news );
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        Log.d("Testing6", list.size() + "");
        return list;
    }

    // delete news
    public void delNew( int id ){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("news", "id =?", new String[]{String.valueOf(id)});
        db.close();
    }

    // update news
    public void updateNew( New news ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", news.getNews_id());
        values.put("feed", news.getFeed());
        values.put("title", news.getTitle());
        values.put("details_url", news.getDetail_url());
        values.put("thumb", news.getThumb_img());
        values.put("description", news.getDescription());
        values.put("author", news.getAuthor());
        values.put("publish_date", news.getPublist_date());
        values.put("created_at", "2019-09-01");
        values.put("updated_at", "2019-09-03");
        // updating row
        db.update("news", values, "id=?", new String[]{String.valueOf(news.getNews_id())});
        db.close();
    }

    // dem so ban ghi
    public int getRecordCount( String table){
        int count = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + table;
        Cursor cursor = db.rawQuery(sql, null);
        count = cursor.getCount();
        db.close();
        return count;
    }

    // users
    public void addUser( User user ){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", user.getId());
        values.put("email", user.getEmail());
        values.put("password", user.getPassword());
        values.put("name", user.getName());
        values.put("created_at", String.valueOf(java.time.LocalDate.now()));
        values.put("updated_at", String.valueOf(java.time.LocalDate.now()));
        // insert user
        db.insert("users", null, values);
        db.close();
    }

    // get a user
    public User getUser( int id ){
        SQLiteDatabase db = this.getReadableDatabase();
        User user = new User();
        Cursor cursor = db.query("users",new String[]{"id", "email", "password", "name"}, "id=?", new String[]{String.valueOf(id)}, null, null, null, null);
        //
        if ( cursor != null ){
            cursor.moveToFirst();
            user.setId( Integer.parseInt(cursor.getString(0)) );
            user.setEmail( cursor.getString(1) );
            user.setPassword( cursor.getString(2) );
            user.setName( cursor.getString(3) );
        }
        db.close();
        return user;
    }

    // get all
    public ArrayList<User> getAllUsers(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<User> userArrayList = new ArrayList<User>();
        String sql = "SELECT id, email, password, name FROM users;";
        // data
        Cursor cursor = db.rawQuery( sql, null );
        if ( cursor.moveToFirst() ){
            do {
                User user = new User();
                user.setId( Integer.parseInt(cursor.getString(0)) );
                user.setEmail( cursor.getString(1) );
                user.setPassword( cursor.getString(2) );
                user.setName( cursor.getString(3) );
                userArrayList.add( user );
            }while ( cursor.moveToNext() );
        }
        return userArrayList;
    }

    // list email
    // get all
    public ArrayList<String> getAllEmail(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> emailList = new ArrayList<String>();
        String sql = "SELECT email FROM users;";
        // data
        Cursor cursor = db.rawQuery( sql, null );
        if ( cursor.moveToFirst() ){
            do {
                String email = cursor.getString(0);
                emailList.add( email );
            }while ( cursor.moveToNext() );
        }
        return emailList;
    }


}
