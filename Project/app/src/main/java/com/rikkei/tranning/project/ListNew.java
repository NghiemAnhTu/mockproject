package com.rikkei.tranning.project;

import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListNew extends AppCompatActivity {
    SwipeRefreshLayout refreshLayout;
    RecyclerView recyclerView;
    List<New> mList = new ArrayList<New>();
    NewAdapter adapter;
//    ArrayAdapter arrayAdapter;
    DBManager dbManager = new DBManager(this);
    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        mList = loadData();
        //dbManager = new DBManager(this);
//        mList = new ArrayList<New>();
//        mList = database.getAllNews();
        Log.d("Testing", dbManager.getRecordCount("news") + "|");

//        for (int i = 0; i < 15; i++ ){
//            New a = new New(i,"name feed " + i, "name title " + i, "name detail_url " + i, "a", "name description " + i, "name author " + i, i);
//            dbManager.addNews( a );
//        }
        Log.d("Testing2", dbManager.getRecordCount("news") + "|");

//        arrayAdapter = new ArrayAdapter(this, R.layout.customer_new, mList);
        refreshLayout = findViewById(R.id.swipeRefresh);
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        adapter = new NewAdapter( mList, this);/**, recyclerView*/
        // load data
//        loadData();
        recyclerView.setAdapter( adapter );
        //adapter.notifyDataSetChanged();
        recyclerView.setLayoutManager( layoutManager );


//        adapter.setLoadMore(new ILoadMore() {
//            @Override
//            public void onLoadMore() {
//                mList.add( null );
//                adapter.notifyItemInserted(mList.size()-1);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mList.remove( mList.size()-1);
//                        adapter.notifyItemRemoved(mList.size());
//                        //
//                        int index = mList.size();
//                        int end = index  + 10;
//                        for ( int i = index; i< end; i++ ){
//                            mList.add(new New(i, "f" + i, "t" + i, "thumb" + i, "des" + i, "a" + i,  i ));
//                        }
//                        adapter.notifyDataSetChanged();
//                        adapter.setLoader();
//                    }
//                }, 3000);
//            }
//        });
//        for ( int i = 100; i < 110; i++ ){
//            New a = new New(i,"name feed " + i, "name title " + i, "name detail_url " + i, "a", "name description " + i, "name author " + i, i);
//            dbManager.addNews( a );
//        }

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed( new Runnable() {
                    @Override
                    public void run() {
                        //adapter = new NewAdapter(loadData(), this);\
                        for ( int i = 30; i < 40; i++ ){
                            New a = new New(i,"name feed " + i, "name title " + i, "name detail_url " + i, "a", "name description " + i, "name author " + i, i);
                            dbManager.addNews( a );
                        }
                        adapter.setList( loadData() );

                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        refreshLayout.setRefreshing(false);

                    }
                }, 4000);
            }
        });


    }

    public ArrayList<New> loadData(){
        ArrayList<New> list = new ArrayList<New>();

//        dbManager.createNews();
        list = dbManager.getAllNews();
        Log.d("Testing4", dbManager.getRecordCount("news") + "|");

        return list;
    }
}
