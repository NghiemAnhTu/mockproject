package com.rikkei.tranning.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class resetPasswordAPI extends AppCompatActivity {
    EditText mEmail;
    Button mbtnResetPassword;
    ImageView mIBackLogin;
    DBManager dbManager;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
        AnhXa();
        mEmail.addTextChangedListener(resetPasswordTextWatcher);
        dbManager = new DBManager(this);
        list = dbManager.getAllEmail();



    }

    private TextWatcher resetPasswordTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String email = mEmail.getText().toString().trim();

            mbtnResetPassword.setEnabled( !email.isEmpty() );

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void AnhXa(){
        mEmail = findViewById(R.id.emailForgotPassword);
        mbtnResetPassword = findViewById(R.id.btnForgotPassword);
        mIBackLogin = findViewById(R.id.imageBackLogin);
    }

    public void BackLogin( View v){
        Intent backLogin = new Intent(this, doLoginAPI.class);
        startActivity(backLogin);
    }

    // reset pass
    public void ResetPassword( View v){
        String email = mEmail.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if ( email.matches(emailPattern) && email.length() > 0 ){

        }else {
            Toast.makeText(resetPasswordAPI.this, "Định dạng Email không đúng, vui lòng nhập lại!", Toast.LENGTH_LONG).show();
        }
        
    }
}
