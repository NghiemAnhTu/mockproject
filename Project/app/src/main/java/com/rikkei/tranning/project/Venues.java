package com.rikkei.tranning.project;

import java.sql.Blob;
import java.sql.Time;

public class Venues {
    String id;
    String name;
    int type;
    String description;
    int permanent;
    String contact_fee;
    String contact_phone;
    String contact_fax;
    String contact_web;
    String contact_web_lang;
    String contact_address;
    String contact_access;
    int contact_discount;
    String contact_discount_details;
    String geo_are;
    int geo_long;
    int geo_lat;
    String schedule_openinghour;
    String schedule_closinghour;
    Boolean schedule_breakstart;
    Boolean schedule_breakend;
    String schedule_openingdetails;
    String schedule_closed;

    //
    public Venues(){}

    public Venues(String id, String name, int type, String description, int permanent, String contact_fee, String contact_phone, String contact_fax, String contact_web, String contact_web_lang, String contact_address, String contact_access, int contact_discount, String contact_discount_details, String geo_are, int geo_long, int geo_lat, String schedule_openinghour, String schedule_closinghour, Boolean schedule_breakstart, Boolean schedule_breakend, String schedule_openingdetails, String schedule_closed) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.permanent = permanent;
        this.contact_fee = contact_fee;
        this.contact_phone = contact_phone;
        this.contact_fax = contact_fax;
        this.contact_web = contact_web;
        this.contact_web_lang = contact_web_lang;
        this.contact_address = contact_address;
        this.contact_access = contact_access;
        this.contact_discount = contact_discount;
        this.contact_discount_details = contact_discount_details;
        this.geo_are = geo_are;
        this.geo_long = geo_long;
        this.geo_lat = geo_lat;
        this.schedule_openinghour = schedule_openinghour;
        this.schedule_closinghour = schedule_closinghour;
        this.schedule_breakstart = schedule_breakstart;
        this.schedule_breakend = schedule_breakend;
        this.schedule_openingdetails = schedule_openingdetails;
        this.schedule_closed = schedule_closed;
    }

    //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPermanent() {
        return permanent;
    }

    public void setPermanent(int permanent) {
        this.permanent = permanent;
    }

    public String getContact_fee() {
        return contact_fee;
    }

    public void setContact_fee(String contact_fee) {
        this.contact_fee = contact_fee;
    }

    public String getContact_phone() {
        return contact_phone;
    }

    public void setContact_phone(String contact_phone) {
        this.contact_phone = contact_phone;
    }

    public String getContact_fax() {
        return contact_fax;
    }

    public void setContact_fax(String contact_fax) {
        this.contact_fax = contact_fax;
    }

    public String getContact_web() {
        return contact_web;
    }

    public void setContact_web(String contact_web) {
        this.contact_web = contact_web;
    }

    public String getContact_web_lang() {
        return contact_web_lang;
    }

    public void setContact_web_lang(String contact_web_lang) {
        this.contact_web_lang = contact_web_lang;
    }

    public String getContact_address() {
        return contact_address;
    }

    public void setContact_address(String contact_address) {
        this.contact_address = contact_address;
    }

    public String getContact_access() {
        return contact_access;
    }

    public void setContact_access(String contact_access) {
        this.contact_access = contact_access;
    }

    public int getContact_discount() {
        return contact_discount;
    }

    public void setContact_discount(int contact_discount) {
        this.contact_discount = contact_discount;
    }

    public String getContact_discount_details() {
        return contact_discount_details;
    }

    public void setContact_discount_details(String contact_discount_details) {
        this.contact_discount_details = contact_discount_details;
    }

    public String getGeo_are() {
        return geo_are;
    }

    public void setGeo_are(String geo_are) {
        this.geo_are = geo_are;
    }

    public int getGeo_long() {
        return geo_long;
    }

    public void setGeo_long(int geo_long) {
        this.geo_long = geo_long;
    }

    public int getGeo_lat() {
        return geo_lat;
    }

    public void setGeo_lat(int geo_lat) {
        this.geo_lat = geo_lat;
    }

    public String getSchedule_openinghour() {
        return schedule_openinghour;
    }

    public void setSchedule_openinghour(String schedule_openinghour) {
        this.schedule_openinghour = schedule_openinghour;
    }

    public String getSchedule_closinghour() {
        return schedule_closinghour;
    }

    public void setSchedule_closinghour(String schedule_closinghour) {
        this.schedule_closinghour = schedule_closinghour;
    }

    public Boolean getSchedule_breakstart() {
        return schedule_breakstart;
    }

    public void setSchedule_breakstart(Boolean schedule_breakstart) {
        this.schedule_breakstart = schedule_breakstart;
    }

    public Boolean getSchedule_breakend() {
        return schedule_breakend;
    }

    public void setSchedule_breakend(Boolean schedule_breakend) {
        this.schedule_breakend = schedule_breakend;
    }

    public String getSchedule_openingdetails() {
        return schedule_openingdetails;
    }

    public void setSchedule_openingdetails(String schedule_openingdetails) {
        this.schedule_openingdetails = schedule_openingdetails;
    }

    public String getSchedule_closed() {
        return schedule_closed;
    }

    public void setSchedule_closed(String schedule_closed) {
        this.schedule_closed = schedule_closed;
    }
}
