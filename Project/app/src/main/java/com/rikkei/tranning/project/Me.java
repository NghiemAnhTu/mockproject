package com.rikkei.tranning.project;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Me extends AppCompatActivity {
    EditText mName, mEmail, mPassword;
    TextView txtAMember;
    Button mbtnSignup;
    int flag = 0;
    int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (flag == 0) {
            setContentView(R.layout.sign_up);
            AnhXa();
            mName.addTextChangedListener(SignUpTextWatcher);
            mEmail.addTextChangedListener(SignUpTextWatcher);
            mPassword.addTextChangedListener(SignUpTextWatcher);
//            mbtnSignup.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String name = mName.getText().toString();
//                    String email = mEmail.getText().toString();
//                    String password = mPassword.getText().toString();
//                    if ( name.length() > 0 && email.length() > 0 && password.length() >= 6 ){
////                        mbtnSignup.setVisibility(View.VISIBLE);
////                        mbtnSignup.setClickable(true);
////                        mbtnSignup.setEnabled(true);
//                        setContentView(R.layout.activity_home);
//                        flag = 1;
//                        Log.d("aa", name + "| " + email + "|" + password);
//                    }else {
//                        Toast.makeText(Me.this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_LONG).show();
//                    }
//                }
//            });
//            Log.d("aa", "Àafadsfas");
//

        }else {
            setContentView(R.layout.activity_me);
        }

    }

    private TextWatcher SignUpTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String name = mName.getText().toString().trim();
            String email = mEmail.getText().toString().trim();
            String password = mPassword.getText().toString().trim();

            mbtnSignup.setEnabled( !name.isEmpty() && !email.isEmpty() && !password.isEmpty());

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public void AnhXa(){
        txtAMember = findViewById(R.id.textAMember);
        mName = findViewById(R.id.fullname);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mbtnSignup = findViewById(R.id.btnSignUp);
    }

    // have a acount
    public void AMember(View v){
        Intent login = new Intent(Me.this, doLoginAPI.class);
        startActivity(login);
    }

    // sign up
    public void SignUp( View v ){
        String name = mName.getText().toString();
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        if ( name.length() > 0 && email.length() > 0 && password.length() >= 6 ){
            setContentView(R.layout.activity_home);
            flag = 1;
            Log.d("aa", name + "| " + email + "|" + password);
        }else {
            Toast.makeText(Me.this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_LONG).show();
        }
    }

}