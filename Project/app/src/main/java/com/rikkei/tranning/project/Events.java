package com.rikkei.tranning.project;

public class Events {
    int id;
    int status;
    String link;
    String photo;
    String name;
    String description_raw;
    String description_html;
    String artist;
    String artist_ect;
    String image_copyright;
    String one_category_link;
    String one_category_name;
    String url;
    String fee_description;
    int fee_discount;
    String fee_discount_details;
    String schedule_permanent;
    String schedule_date_warning;
    String schedule_time_alert;
    String schedule_start_date;
    String schedule_start_time;
    String schedule_end_date;
    String schedule_end_time;
    String schedule_one_day_event;
    String schedule_extra;
    String venue_id;

    //
    public Events(){}

    public Events(int id, int status, String link, String photo, String name, String description_raw, String description_html, String artist, String artist_ect, String image_copyright, String one_category_link, String one_category_name, String url, String fee_description, int fee_discount, String fee_discount_details, String schedule_permanent, String schedule_date_warning, String schedule_time_alert, String schedule_start_date, String schedule_start_time, String schedule_end_date, String schedule_end_time, String schedule_one_day_event, String schedule_extra, String venue_id) {
        this.id = id;
        this.status = status;
        this.link = link;
        this.photo = photo;
        this.name = name;
        this.description_raw = description_raw;
        this.description_html = description_html;
        this.artist = artist;
        this.artist_ect = artist_ect;
        this.image_copyright = image_copyright;
        this.one_category_link = one_category_link;
        this.one_category_name = one_category_name;
        this.url = url;
        this.fee_description = fee_description;
        this.fee_discount = fee_discount;
        this.fee_discount_details = fee_discount_details;
        this.schedule_permanent = schedule_permanent;
        this.schedule_date_warning = schedule_date_warning;
        this.schedule_time_alert = schedule_time_alert;
        this.schedule_start_date = schedule_start_date;
        this.schedule_start_time = schedule_start_time;
        this.schedule_end_date = schedule_end_date;
        this.schedule_end_time = schedule_end_time;
        this.schedule_one_day_event = schedule_one_day_event;
        this.schedule_extra = schedule_extra;
        this.venue_id = venue_id;
    }

    // get set

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription_raw() {
        return description_raw;
    }

    public void setDescription_raw(String description_raw) {
        this.description_raw = description_raw;
    }

    public String getDescription_html() {
        return description_html;
    }

    public void setDescription_html(String description_html) {
        this.description_html = description_html;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtist_ect() {
        return artist_ect;
    }

    public void setArtist_ect(String artist_ect) {
        this.artist_ect = artist_ect;
    }

    public String getImage_copyright() {
        return image_copyright;
    }

    public void setImage_copyright(String image_copyright) {
        this.image_copyright = image_copyright;
    }

    public String getOne_category_link() {
        return one_category_link;
    }

    public void setOne_category_link(String one_category_link) {
        this.one_category_link = one_category_link;
    }

    public String getOne_category_name() {
        return one_category_name;
    }

    public void setOne_category_name(String one_category_name) {
        this.one_category_name = one_category_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFee_description() {
        return fee_description;
    }

    public void setFee_description(String fee_description) {
        this.fee_description = fee_description;
    }

    public int getFee_discount() {
        return fee_discount;
    }

    public void setFee_discount(int fee_discount) {
        this.fee_discount = fee_discount;
    }

    public String getFee_discount_details() {
        return fee_discount_details;
    }

    public void setFee_discount_details(String fee_discount_details) {
        this.fee_discount_details = fee_discount_details;
    }

    public String getSchedule_permanent() {
        return schedule_permanent;
    }

    public void setSchedule_permanent(String schedule_permanent) {
        this.schedule_permanent = schedule_permanent;
    }

    public String getSchedule_date_warning() {
        return schedule_date_warning;
    }

    public void setSchedule_date_warning(String schedule_date_warning) {
        this.schedule_date_warning = schedule_date_warning;
    }

    public String getSchedule_time_alert() {
        return schedule_time_alert;
    }

    public void setSchedule_time_alert(String schedule_time_alert) {
        this.schedule_time_alert = schedule_time_alert;
    }

    public String getSchedule_start_date() {
        return schedule_start_date;
    }

    public void setSchedule_start_date(String schedule_start_date) {
        this.schedule_start_date = schedule_start_date;
    }

    public String getSchedule_start_time() {
        return schedule_start_time;
    }

    public void setSchedule_start_time(String schedule_start_time) {
        this.schedule_start_time = schedule_start_time;
    }

    public String getSchedule_end_date() {
        return schedule_end_date;
    }

    public void setSchedule_end_date(String schedule_end_date) {
        this.schedule_end_date = schedule_end_date;
    }

    public String getSchedule_end_time() {
        return schedule_end_time;
    }

    public void setSchedule_end_time(String schedule_end_time) {
        this.schedule_end_time = schedule_end_time;
    }

    public String getSchedule_one_day_event() {
        return schedule_one_day_event;
    }

    public void setSchedule_one_day_event(String schedule_one_day_event) {
        this.schedule_one_day_event = schedule_one_day_event;
    }

    public String getSchedule_extra() {
        return schedule_extra;
    }

    public void setSchedule_extra(String schedule_extra) {
        this.schedule_extra = schedule_extra;
    }

    public String getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(String venue_id) {
        this.venue_id = venue_id;
    }
}
